/**
 * COMFORT SENSOR:
 * 
 */
const axios = require('axios')


 //SETTINGS:
 //Gateway1 (GWID 0020)
 var address = "http://127.0.0.1:8000/machine/state";


 function randomInt(min, max) {
	return min + Math.floor((max - min) * Math.random());
}
function randomFloat(min, max) {
    return min + (max - min) * Math.random();
}

const mock = (msid)=>{
    var tempVal=randomInt(15, 30); // min: 18, max: 28
    var radonVal=randomInt(10,300); //over 230 is bad
    var powerVal=randomInt(1200,2200); //assume that over 2000 is bad

    axios
    .post(address, {
      temperature: tempVal,
      radon: radonVal,
      power:powerVal,
      MSID:msid,
      timestamp: Date.now()
    })
    .then(res => {
      //console.log(res.status + ": " +res.data)
      //console.log(res.data)
    })
    .catch(error => {
      //console.error(error)
    })
    
};

//console.log("Sensor started!");

//send state each minute

//moch for Hall1 (GWID 0020)
setInterval( 
    ()=>{mock(21)}, 60000);

setInterval( 
    ()=>{mock(22)}, 63000);

setInterval( 
    ()=>{mock(31)}, 66000);

setInterval( 
    ()=>{mock(32)}, 69000);